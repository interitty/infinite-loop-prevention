<?php

declare(strict_types=1);

namespace Interitty\InfiniteLoopPrevention;

use Interitty\PhpUnit\BaseTestCase;

use function call_user_func_array;

/**
 * @coversDefaultClass Interitty\InfiniteLoopPrevention\InfiniteLoopPrevention
 */
class InfiniteLoopPreventionTest extends BaseTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">

    /**
     * Tester of checkFinite implementation in infinite function
     *
     * @return void
     * @group integration
     * @covers ::checkFinite
     * @covers ::isFinite
     */
    public function testCheckFiniteInFunction(): void
    {
        $infinite = static function (callable $callback): void {
            InfiniteLoopPrevention::checkFinite();
            call_user_func_array($callback, [$callback]);
        };
        $callback = static function (): void {
        };

        call_user_func_array($infinite, [$callback]);
        call_user_func_array($infinite, [$callback]);
        $this->expectException(InfiniteLoopException::class);
        call_user_func_array($infinite, [$infinite]);
    }

    /**
     * Tester of checkFinite implementation in two infinite functions
     *
     * @return void
     * @group integration
     * @covers ::checkFinite
     * @covers ::isFinite
     */
    public function testCheckFiniteInTwoFunctions(): void
    {
        $infiniteOne = static function (callable $infiniteTwo, callable $infiniteOne): void {
            InfiniteLoopPrevention::checkFinite();
            call_user_func_array($infiniteTwo, [$infiniteOne, $infiniteTwo]);
        };
        $infiniteTwo = static function (callable $infiniteOne, callable $infiniteTwo): void {
            call_user_func_array($infiniteOne, [$infiniteTwo, $infiniteOne]);
        };

        $this->expectException(InfiniteLoopException::class);
        call_user_func_array($infiniteOne, [$infiniteTwo, $infiniteOne]);
    }

    /**
     * Tester of checkFinite implementation in method
     *
     * @return void
     * @group integration
     * @covers ::checkFinite
     * @covers ::isFinite
     */
    public function testCheckFiniteInMethod(): void
    {
        $this->expectException(InfiniteLoopException::class);
        InfiniteLoopPrevention::checkFinite();
        $this->testCheckFiniteInMethod();
    }
    // </editor-fold>
}
