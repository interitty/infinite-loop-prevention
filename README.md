# Infinite loop prevention #

A simple infinite loop prevention tool that help developers to prevent the error `Maximum function nesting level of '256' reached, aborting!`.

## Requirements ##

- [PHP](https://php.net/) >= 8.3

## Installation ##

The best way to install [**interitty/infinite-loop-prevention**](https://gitlab.com/interitty/infinite-loop-prevention) is using [Composer](https://getcomposer.org/):

```bash
composer require interitty/infinite-loop-prevention
```

## Usage ##

For cases when an infinite loop can happen, especially when code work with a given callback,
it can be handy to call preventive assertion in a non-production environment just to be sure,
that code can't fall into an infinite loop.

Because the code is based on [`debug_backtrace`](https://www.php.net/manual/en/function.debug-backtrace.php),
it will be better to disable the mechanism on the production environment by setting the static
`InfiniteLoopPrevention::$productionMode` variable.

For better performance in the production environment, it will be better to use the [`assert`](https://www.php.net/manual/en/function.assert.php),
because the execution and whole code generating can be disabled by the [`zend.assertions`](https://www.php.net/manual/en/function.assert.php)
in the `php.ini` file.

It only needs to call the static method without any parameters just inside the method or function where the given
callback is called.

### Example: Usage of checkFinite() ###

```php

function test(): void
{
    assert(InfiniteLoopPrevention::checkFinite());
    test();
}

test();
```
