<?php

declare(strict_types=1);

namespace Interitty\InfiniteLoopPrevention;

use LogicException;

class InfiniteLoopException extends LogicException
{
}
