<?php

declare(strict_types=1);

namespace Interitty\InfiniteLoopPrevention;

use function array_key_exists;
use function debug_backtrace;
use function implode;
use function spl_object_hash;

use const DEBUG_BACKTRACE_IGNORE_ARGS;
use const DEBUG_BACKTRACE_PROVIDE_OBJECT;

class InfiniteLoopPrevention
{
    /** @var bool */
    public static bool $productionMode = false;

    /**
     * Throws exception if infinite loop is detected on non-production mode
     *
     * @return bool
     */
    public static function checkFinite(): bool
    {
        if ((self::$productionMode === false) && (self::isFinite() !== true)) {
            throw new InfiniteLoopException();
        }
        return true;
    }

    /**
     * Finite checker
     *
     * @return bool
     */
    public static function isFinite(): bool
    {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS);
        $deepLevel = 0;
        $isInfinite = true;
        $items = [];
        /* @var array{file: string, line: int, function: string, class: string, object: string, type: string} $step */
        foreach ($backtrace as $step) {
            unset($step['args']);
            if ((array_key_exists('class', $step) === true) && ($step['class'] === __CLASS__)) {
                continue;
            }

            $deepLevel = $deepLevel + 1;
            $step['object'] = array_key_exists('object', $step) ? spl_object_hash($step['object']) : '';
            $stepId = implode($step);

            if (($deepLevel > 1) && (array_key_exists($stepId, $items) === true)) {
                $isInfinite = false;
                break;
            }
            $items[$stepId] = true;
        }

        return $isInfinite;
    }
}
